<?php return [
    // This contains the Laravel Packages that you want this plugin to utilize listed under their package identifiers
    'packages' => [
        'maatwebsite/excel' => [
            // Service providers to be registered by your plugin
            'providers' => [
                Maatwebsite\Excel\ExcelServiceProvider::class,
            ],

            // Aliases to be registered by your plugin in the form of $alias => $pathToFacade
            'aliases' => [
                'Excel' => Maatwebsite\Excel\Facades\Excel::class,
            ],
        ],
    ],
];
