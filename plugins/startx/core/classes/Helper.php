<?php namespace Startx\Core\Classes;

/**
 *
 */
class Helper
{
	public function initUnit()
	{
		$parents   = \Simantap\Unit\Models\Unit::whereIn('id', [1,2,3])->get();
        $districts = \Startx\Location\Models\District::whereRegencyId('3673')->get();
        $inScope   = ['pemkot', 'police'];
        foreach ($parents as $key => $parent) {
            foreach ($districts as $key => $district) {
                $villages = \Startx\Location\Models\Village::whereDistrictId($district->id)->get();

                if($parent->code == 'pemkot') {
                    $unt = \Simantap\Unit\Models\Unit::firstOrNew([
                        'parent_id' => $parent->id,
                        'name'      => 'Kecamatan '.ucwords(strtolower($district->name))
                    ]);
                    $unt->district_id  = $district->id;
                    $unt->village_id   = $district->villages[0]->id;
                    $unt->save();
                }

                if($parent->code == 'police') {
                    $unt = \Simantap\Unit\Models\Unit::firstOrNew([
                        'parent_id' => $parent->id,
                        'name'      => 'Polsek '.ucwords(strtolower($district->name))
                    ]);
                    $unt->district_id  = $district->id;
                    $unt->village_id   = $district->villages[0]->id;
                    $unt->save();
                }

                if($parent->code == 'tni') {
                    $unt = \Simantap\Unit\Models\Unit::firstOrNew([
                        'parent_id' => $parent->id,
                        'name'      => 'Danrem '.ucwords(strtolower($district->name))
                    ]);
                    $unt->district_id  = $district->id;
                    $unt->village_id   = $district->villages[0]->id;
                    $unt->save();
                }

                foreach ($villages as $key => $village) {

                    if($parent->code == 'pemkot') {
                        $unit = \Simantap\Unit\Models\Unit::firstOrNew([
                            'parent_id' => $unt->id,
                            'name'      => 'Kelurahan '.ucwords(strtolower($village->name)),
                        ]);
                        $unit->district_id  = $district->id;
                        $unit->village_id   = $village->id;
                        $unit->save();
                    }

                    if($parent->code == 'police') {
                        $unit = \Simantap\Unit\Models\Unit::firstOrNew([
                            'parent_id' => $unt->id,
                            'name'      => 'Babinkamtibnas '.ucwords(strtolower($village->name)),
                        ]);
                        $unit->district_id  = $district->id;
                        $unit->village_id   = $village->id;
                        $unit->save();
                    }

                    if($parent->code == 'tni') {
                        $unit = \Simantap\Unit\Models\Unit::firstOrNew([
                            'parent_id' => 3,
                            'name'      => 'Babinsa '.ucwords(strtolower($village->name)),
                        ]);
                        $unit->district_id  = $district->id;
                        $unit->village_id   = $village->id;
                        $unit->save();
                    }
                }
            }
        }
	}

	public function dateToRomawi($date)
	{
		$month= \Carbon\Carbon::parse($date)->format('m');
		switch ($month) {
			case '01':
				$month = 'I';
				break;
			case '02':
				$month = 'II';
				break;
			case '03':
				$month = 'III';
				break;
			case '04':
				$month = 'IV';
				break;
			case '05':
				$month = 'V';
				break;
			case '06':
				$month = 'VI';
				break;
			case '07':
				$month = 'VII';
				break;
			case '08':
				$month = 'VIII';
				break;
			case '09':
				$month = 'IX';
				break;
			case '10':
				$month = 'X';
				break;
			case '11':
				$month = 'XI';
				break;
			case '12':
				$month = 'XII';
				break;
			default:
				break;
		}

		return $month;
	}

	public function transformOrganizationStatus($status)
	{
		switch ($status) {
			case 'draft':
				return [
					'value' => 'draft',
					'text'  => 'Arsip',
					'client'=> 'Proses kelengkapan berkas'
				];
				break;

			case 'wait':
				return [
					'value' => 'wait',
					'text'  => 'Menunggu',
					'client'=> 'Proses pengecekan berkas'
				];
				break;

			case 'process':
				return [
					'value' => 'process',
					'text'  => 'Proses',
					'client'=> 'Proses pengecekan petugas'
				];
				break;

			case 'success':
				return [
					'value' => 'success',
					'text'  => 'Sukses',
					'client'=> 'Terkonfirmasi'
				];
				break;

			default:
				break;
		}
	}

	public function dateToText($date)
	{
		$day 	= \Carbon\Carbon::parse($date)->format('d');
		$month  = \Carbon\Carbon::parse($date)->format('m');
		$year   = \Carbon\Carbon::parse($date)->format('Y');

		/**
		 * Day
	 	*/
		switch ($day) {
			case '1':
				$dayText = 'Satu';
				break;
			case '2':
				$dayText = 'Dua';
				break;
			case '3':
				$dayText = 'Tiga';
				break;
			case '4':
				$dayText = 'Empat';
				break;
			case '5':
				$dayText = 'Lima';
				break;
			case '6':
				$dayText = 'Enam';
				break;
			case '7':
				$dayText = 'Tujuh';
				break;
			case '8':
				$dayText = 'Delapan';
				break;
			case '9':
				$dayText = 'Sembilan';
				break;
			case '10':
				$dayText = 'Sepuluh';
				break;
			case '11':
				$dayText = 'Sebelas';
				break;
			case '12':
				$dayText = 'Dua Belas';
				break;
			case '13':
				$dayText = 'Tiga Belas';
				break;
			case '14':
				$dayText = 'Empat Belas';
				break;
			case '15':
				$dayText = 'Lima Belas';
				break;
			case '16':
				$dayText = 'Enam Belas';
				break;
			case '17':
				$dayText = 'Tujuh Belas';
				break;
			case '18':
				$dayText = 'Delapan Belas';
				break;
			case '19':
				$dayText = 'Sembilan Belas';
				break;
			case '20':
				$dayText = 'Dua Puluh';
				break;
			case '21':
				$dayText = 'Dua Puluh Satu';
				break;
			case '22':
				$dayText = 'Dua Puluh dua';
				break;
			case '23':
				$dayText = 'Dua Puluh Tiga';
				break;
			case '24':
				$dayText = 'Dua Puluh Empat';
				break;
			case '25':
				$dayText = 'Dua Puluh Lima';
				break;
			case '26':
				$dayText = 'Dua Puluh Enam';
				break;
			case '27':
				$dayText = 'Dua Puluh Tujuh';
				break;
			case '28':
				$dayText = 'Dua Puluh Delapan';
				break;
			case '29':
				$dayText = 'Dua Puluh Sembilan';
				break;
			case '30':
				$dayText = 'Tiga Puluh';
				break;
			case '31':
				$dayText = 'Tiga Puluh Satu';
				break;
			default:
				break;
		}

		/**
		 * Month
	 	*/
		switch ($month) {
			case '01':
				$monthText = 'Januari';
				break;
			case '02':
				$monthText = 'Februari';
				break;
			case '03':
				$monthText = 'Maret';
				break;
			case '04':
				$monthText = 'April';
				break;
			case '05':
				$monthText = 'Mei';
				break;
			case '06':
				$monthText = 'Juni';
				break;
			case '07':
				$monthText = 'Juli';
				break;
			case '08':
				$monthText = 'Agustus';
				break;
			case '09':
				$monthText = 'September';
				break;
			case '10':
				$monthText = 'Oktober';
				break;
			case '11':
				$monthText = 'November';
				break;
			case '12':
				$monthText = 'Desember';
				break;
			default:
				break;
		}

		/**
		 * Year
	 	*/
		switch ($year) {
			case '2021':
				$yearText = 'Dua Ribu Dua Puluh Satu';
				break;
			case '2022':
				$yearText = 'Dua Ribu Dua Puluh Dua';
				break;
			case '2023':
				$yearText = 'Dua Ribu Dua Puluh Tiga';
				break;
			case '2024':
				$yearText = 'Dua Ribu Dua Puluh Empat';
				break;
			case '2025':
				$yearText = 'Dua Ribu Dua Puluh Lima';
				break;
			default:
				break;
		}

		return strtoupper('Tanggal '.$dayText.' Bulan '.$monthText.' Tahun '.$yearText);
	}

	public function transformgender($gender)
	{
		switch ($gender) {
            case 'male':
                return [
                    'value' => 'male',
                    'name'  => 'Laki-Laki'
                ];
                break;

            case 'female':
                return [
                    'value' => 'female',
                    'name'  => 'Perempuan'
                ];
                break;

            default:
                break;
        }
	}

	public function transformDate($date)
	{
		if($date) {
			$date = \Carbon\Carbon::parse($date);
			return [
				'l' 		=> $date->format('l'),
				'ymd' 		=> $date->format('Y-m-d'),
				'period'    => $date->format('F Y'),
				'dFY' 		=> $date->format('d F Y'),
				'Hi' 		=> $date->format('H:i'),
				'picker'	=> $date->format('Y, m, d')
			];
		}

		return false;
	}

	public function transformPrice($value)
	{
		return [
			'text' => number_format($value),
			'value'=> (int) $value,
			'idr'  => 'Rp. '.number_format($value)
		];
	}

	public function transformComma($data, $name)
	{
		$word = '';
		foreach ($data as $key => $dat) {
			if(count($data) == ($key+1)) {
				$word .= $dat[$name];
			}
			else {
				$word .= $dat[$name].', ';
			}
		}

		return $word;
	}
}
