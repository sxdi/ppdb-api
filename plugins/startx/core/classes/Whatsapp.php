<?php namespace Startx\Core\Classes;

/**
 *
 */
class Whatsapp
{
    public function reformat($receiver)
    {
        $code = '08';
        $intr = '628';
        $peach= substr($receiver, 0, 2);

        if($peach == $code) {
            $receiver = str_replace($code, $intr, $receiver);
        }

        return $receiver;
    }

	public function send($receiver, $msg, $empty=null)
	{
        if(!$empty) {
            $message  = 'KESBANGPOL KOTA SERANG — '.$msg;
        }
        else {
            $message = $msg;
        }
        $data     = [
            'text'     => $message,
            'number'   => $this->reformat($receiver),
        ];

        $pulse = file_get_contents(env('APP_WHATSAPP').'?'.http_build_query($data));
        return json_decode($pulse, true);
	}
}
