<?php namespace Startx\Api\Transformers;

use Ppdb\School\Models\School;
use League\Fractal\TransformerAbstract;

class SchoolTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(School $school)
    {
        $helper = new \Startx\Core\Classes\Helper;
        return [
            'id'        => $school->id,
            'address'   => $school->address,
            'name'      => $school->name,
            'hoby'      => $school->hoby,
            'create'    => $helper->transformDate($school->created_at),
            'update'    => $helper->transformDate($school->updated_at),
        ];
    }
}
