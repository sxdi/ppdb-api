<?php namespace Startx\Api\Transformers;

use Ppdb\Test\Models\Test;
use League\Fractal\TransformerAbstract;

class TestTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Test $test)
    {
        $helper = new \Startx\Core\Classes\Helper;
        return [
            'id'    => $test->id,
            'name'  => $test->name,
            'create' => $helper->transformDate($test->created_at),
        ];
    }
}
