<?php namespace Startx\Api\Transformers;

use System\Models\File;
use League\Fractal\TransformerAbstract;

class FileTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(File $file)
    {
        $helper = new \Startx\Core\Classes\Helper;
        return [
            'id'     => $file->id,
            'name'   => $file->file_name,
            'size'   => $file->file_size,
            'type'   => $file->content_type,
            'ext'    => $file->extension,
            'create' => $helper->transformDate($file->created_at),
            'path'   => $file->path,
        ];
    }
}
