<?php return [
    // This contains the Laravel Packages that you want this plugin to utilize listed under their package identifiers
    'packages' => [
        'tymon/jwt-auth' => [
            // Service providers to be registered by your plugin
            'providers' => [
                '\Tymon\JWTAuth\Providers\LaravelServiceProvider',
            ],

            // Aliases to be registered by your plugin in the form of $alias => $pathToFacade
            'aliases' => [
                'JWTAuth'       => '\Tymon\JWTAuth\Facades\JWTAuth',
                'JWTFactory'    => '\Tymon\JWTAuth\Facades\JWTFactory',
            ],

            // The namespace to set the configuration under. For this example, this package accesses it's config via config('purifier.' . $key), so the namespace 'purifier' is what we put here
            // 'config_namespace' => 'purifier',

            // The configuration file for the package itself. Start this out by copying the default one that comes with the package and then modifying what you need.
        ],
    ],
];
