<?php namespace Startx\Api\Controllers;

use Octobro\API\Classes\ApiController;

class Tests extends ApiController
{
    public function get()
    {
        $tests  = \Ppdb\Test\Models\Test::get();
        $filter = input('filter');
        if($filter) {
            /**
             * By Kode
            */
            // if (isset($filter['code']) && $filter['code']) {
            //     $tests = $tests->where('code', 'like', '%'.$filter['code'].'%');
            // }
        }

        $order    = input('order');
        if($order) {
            /**
             * By Code
            */
            // if (isset($order['code']) && $order['code']) {
            //     $tests = $tests->orderBy('code', $order['code']);
            // }
        }

        return response()->json([
            'result' => true,
            'response' => $this->respondWithCollection($tests, new \Startx\Api\Transformers\TestTransformer)
        ]);
    }
}
