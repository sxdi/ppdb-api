<?php namespace Startx\Api\Controllers;

use Ppdb\School\Models\School;
use Octobro\API\Classes\ApiController;

class Schools extends ApiController
{
    public function get()
    {
        $schools  = new School;
        $filter   = input('filter');
        if ($filter) {
            /**
             * By Name
            */
            if (isset($filter['name']) && $filter['name']) {
                $names = explode(',', $filter['name']);
                foreach ($names as $name) {
                    $schools = $schools->orWhere('name', 'like', '%'.$name.'%');
                }
            }
        }

        $order    = input('order');
        if ($order) {
            /**
             * By Code
            */
            if (isset($order['name']) && $order['name']) {
                $schools = $schools->orderBy('name', $order['name']);
            }
        }

        $schools = $schools->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($schools, new \Startx\Api\Transformers\SchoolTransformer)
        ]);
    }
    
    public function store()
    {
        $email      = request('email');
        $checkData  = School::where('email', '=', $email)->get();

        # code...
        School::updateOrCreate(
            ['email' =>  request('email')],
            [
                'name'    => request('name'),
                'address' => request('address'),
                'hoby'    => request('hoby'),
                'phone'   => request('phone')
            ]
        );

        $response = $checkData->count()>=1?'Successfully Updated Data '.$email.' !':'Successfully Created Data '.$email.'!' ;

        return response()->json([
            'result'    => true,
            'response'  => $response
        ]);
    }

    public function destroy()
    {
        $filter = input('delete');
        $schools= new School;

        if (isset($filter['email']) && $filter['email']) {
            $explodeFilter = explode(',', $filter['email']);
            foreach ($explodeFilter as $keyFilter => $valueFilters) {
                $delete = $schools->orWhere('email', '=', $valueFilters)->delete();
            }
        }

        if ($delete) {
            return response()->json([
                 'result' => true,
                 'response' => 'delete record successfully!'
             ]);
        } else {
            return response([
                'result' => false,
                'response' => 'failed deleted record!'
         ]);
        }
    }
}
