<?php namespace Startx\Api\Classes;

class Notification
{
    public function pulse($data)
    {
        $token   = $data['token'];
        $body    = $data['body'];
        $fcmUrl  = 'https://fcm.googleapis.com/fcm/send';

        if(isset($data['app'])) {
            $headers = [
                'Authorization: key=AAAAnmciWzA:APA91bHJpkVuq8zfmbWfS3ccPyzR4oyDwjKWWjK3DQAa3Sm_vToN8E4erv1u7n5KP8EyfyZ4241QKKxfPtxrp2YGKCHmLPFM4KydfEfyLeQkshsgPT-ossAS1iDNVy42KFJDm8tTe_D8',
                'Content-Type: application/json'
            ];
        }
        else {
            $headers = [
                'Authorization: key=AAAArfr_LLA:APA91bHYq1yRlnML_MJZMZOizS4eWs3ozpVAQU1VnGHNMqKPiRoQ-Lbgc6dT8x_sdEO9jF6TwasmlAhOa4T7IdQ5gZCWdGeQ7zh70uZvOj5VsaIrfWK82kiHd0sglbwYeinoMkOM9jA9',
                'Content-Type: application/json'
            ];
        }

        $notification = [
            'title'              => isset($body['title']) ? $body['title'] : 'Pemberitahuan',
            'body'               => $body['body'],
            'sound'              => 'bell',
            'color'              => '#222',
            'priority'           => 'high',
            'icon'               => 'ic_launcher',
            'show_in_foreground' => true,
            'show_in_background' => true,
        ];

        if(isset($body['application'])) {
            $notification['application'] = $body['application'];
        }

        $fcmNotification = [
            'registration_ids' => $token,
            'notification'     => $notification,
            'data'             => $notification
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }
}
