<?php namespace Startx\Api\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AndroidPulse extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'startx:android-pulse';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    public function sdd()
    {
        $data = ['Profesi', 'Kegiatan', 'Agama', 'Fungsi', 'LSM', 'Kepercayaan terhadap Tuhan YME', 'Lain-lain'];
        foreach ($data as $key => $d) {
            \Kesbangpol\Category\Models\Category::create([
                'name' => $d
            ]);
        }
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->sdd();
        return;
        $tokens  = 'f72feJeD0WM:APA91bF4EQfak2HcsB_iSZJ_E5Qados9xhj_pKLoeWDii-EeLaaCMaVpN-l7FUmw5QaA_Mv4nz6qWec9lO7mqHLVFotRhp-8rrKj1CaY0vY7ulr2LUYMJjAU5cVjdh2B4DweqfwBpQkz';
        $org     = \Kesbangpol\Organization\Models\Organization::whereId(2)->first();
        $tokens  = \Startx\User\Models\Token::pluck('token');

        $dd      = $org->toJson();
        $notif   = new \Startx\Api\Classes\Notification;
        $body    = 'Testing sent notification to';
        $content = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'router'    => 'RegistrationStack',
                'screen'    => 'RegistrationDetail',
                'parameter' => 'organization',
                'key'       => $dd,
            ]
        ];
        $notif->pulse(['token' => $tokens, 'body' => $content]);
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
