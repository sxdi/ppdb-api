<?php

Route::get('/', function() {
    return;
});

Route::group([
	'prefix'     => 'api/v1',
	'namespace'  => 'Startx\Api\Controllers',
    // 'middleware' => 'cors'
], function() {

    Route::get('android', function() {
        return response()->json([
            'result' => true,
            'client' => env('CLIENT_APP')
        ]);
    });

    Route::group(['prefix' => 'test'], function(){
        Route::get('/', 'Tests@get');
    });

    Route::group(['prefix' => 'school'], function(){
        Route::get('/', 'Schools@get');
        Route::post('/', 'Schools@store');
        Route::delete('/', 'Schools@destroy');
    });
});
