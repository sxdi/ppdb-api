<?php namespace Ppdb\School\Updates;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
class CreateSchoolsTable extends Migration
{
    public function up()
    {
        Schema::create('ppdb_school_schools', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('email');
            $table->string('name');
            $table->string('address');
            $table->string('hoby');
            $table->string('phone');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('ppdb_school_schools');
    }
}
