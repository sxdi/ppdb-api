<?php namespace Ppdb\School;

use Backend;
use App;
use System\Classes\PluginBase;

/**
 * School Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'School',
            'description' => 'No description provided yet...',
            'author'      => 'Ppdb',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        App::register('Tymon\JWTAuth\Providers\JWTAuthServiceProvider');

    }
    
    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        App::register('Tymon\JWTAuth\Providers\JWTAuthServiceProvider');

        // App::register('Tymon\JWTAuth\Providers\LaravelServiceProvider');
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Ppdb\School\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'ppdb.school.some_permission' => [
                'tab' => 'School',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'school' => [
                'label'       => 'School',
                'url'         => Backend::url('ppdb/school/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['ppdb.school.*'],
                'order'       => 500,
            ],
        ];
    }
}
